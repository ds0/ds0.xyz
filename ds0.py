from flask import Flask, render_template, render_template_string, send_from_directory
import os
import itertools
app = Flask(__name__)

@app.route('/')
def home():
    posts = getPosts()
    # send the list of posts backwards (by using [::-1])
    return render_template('show_posts.html', posts=posts[::-1], page=1)

@app.route('/page/<num>')
def page(num):
    try:
        num = int(num)
        if num < 1:
            return render_template('404.html')
        posts = getPosts(10*(int(num)-1))
        if posts:
            return render_template('show_posts.html', posts=posts[::-1], page=num)
        else:
            return render_template('404.html')
    except:
        return render_template('404.html')

@app.route('/about')
def about():
    return render_template('about.html', title="about")

@app.route('/mixes')
def mixes():
    return render_template('mixes.html', title="mixes")

@app.route('/posts/<path:path>')
def render_post(path):
    post = getPost(path)
    if post is not None:
        posts=[post]
        return render_template('show_posts.html', posts=posts, title=post['title'])
    else:
        return render_template('404.html')

@app.route('/archive')
def archive():
    titles = getAllTitles()
    return render_template('archive.html', titles=titles[::-1], title="archive")

@app.route('/media/<path:path>')
def media(path):
    if os.path.isfile('templates/media/'+path):
        path_split = path.rsplit('/',1) if '/' in path else None
        filename = path_split[1] if path_split else path
        path = path_split[0] if path_split else ""
        return send_from_directory(directory='templates/media/'+path, filename=filename)
    else:
        return render_template('404.html')

@app.errorhandler(404)
def page_not_found(e):
    return render_template('404.html'), 404

def getPost(path):
    if os.path.isfile(app.template_folder+'/posts/'+path) and path != '.gitignore':
        post = {}
        post['title'] = path
        post['content'] = None
        with open(app.template_folder+'/posts/'+path, 'r') as f:
            post['content'] = f.read()
        return post
    else:
        return None

def getPosts(start=0, num=10):
    walk = os.walk(app.template_folder + '/posts')
    file_list = []
    for path in walk:
        if path[2] and '.gitignore' not in path[2]:
            file_list += [path]
    file_list = file_list[start:start+num]
    print(file_list)
    posts = []
    for path in file_list:
        post = {}
        post['title'] = path[0][16:] + '/' + path[2][0]
        post['content'] = None
        with open(path[0]+'/'+path[2][0], 'r') as f:
            post['content'] = f.read()
        posts += [post]
    # sort list of posts returned
    posts = sorted(posts, key=lambda k: k['title'])
    return posts

def getAllPosts():
    posts = []
    for path in os.walk(app.template_folder + '/posts'):
        # select only the results of walk() that have files
        if path[2] and '.gitignore' not in path[2]:
            post = {}
            # extract title from the folder path (20XX/XX/XX/) + the file's name
            post['title'] = path[0][16:] + '/' + path[2][0]
            post['content'] = None
            with open(path[0]+'/'+path[2][0], 'r') as f:
                post['content'] = f.read()
            posts += [post]
    return posts

def getAllTitles():
    titles = []
    for path in os.walk(app.template_folder + '/posts'):
        if path[2] and '.gitignore' not in path[2]:
            titles += [path[0][16:] + '/' + path[2][0]]
    print(titles)
    titles = sorted(titles)
    return titles

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=False)

